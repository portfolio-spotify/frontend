const getUserLocale = () => {
    const lang = navigator.language;

    const initials = lang.split('-');

    return initials[0];
};

const formatDateFR = (date) => {
    const options = { year: 'numeric', month: 'long', day: 'numeric' };

    const exactDate = date.toLocaleDateString('fr-FR', options);

    return exactDate;
};

export const formatDate = (date) => {
    const jsDate = new Date(date);
    const locale = getUserLocale();

    if (locale === 'fr') return formatDateFR(jsDate);

    // format date EN
};

export const formatDuration = (duration) => { // in seconds
    const durationFloat = Math.trunc(parseFloat(duration));

    const seconds = durationFloat % 60;
    const minutes = Math.trunc(durationFloat / 60);
    const hours = Math.trunc(minutes / 60);

    const secondsFormat = seconds < 10 ? `0${seconds}` : `${seconds}`;
    let minutesFormat = null;
    let hoursFormat = null;

    if (minutes < 60) minutesFormat = `${minutes}`;

    if (minutes < 10 && hours > 0) minutesFormat = `0${minutes}`;

    if (minutes >= 60) minutesFormat = `${minutes % 60}`;

    if (hours > 0) hoursFormat = `${hours}`;

    if (hours) return `${hoursFormat}:${minutesFormat}:${secondsFormat}`;

    return `${minutesFormat}:${secondsFormat}`;
};

export const formatTotalDuration = (totalDuration) => {
    const date = new Date(null);

    date.setSeconds(totalDuration);

    if (totalDuration < 3600) {
        const duration = date.toISOString().substring(14, 19);
        const units = duration.split(':');

        const minutes = parseInt(units[0], 10);
        const seconds = parseInt(units[1], 10);

        const minutesFormat = minutes > 0 ? `${minutes} min` : '';

        return `${minutesFormat} ${seconds} s`;
    }

    const duration = date.toISOString().substring(11, 16);
    const units = duration.split(':');

    const hours = parseInt(units[0], 10);
    const minutes = parseInt(units[1], 10);

    return `${hours} h ${minutes} min`;
};
