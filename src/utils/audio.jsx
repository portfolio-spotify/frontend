import capitalize from 'lodash/capitalize';

export const getAudioRepeatMode = (currentMode) => {
    if (currentMode === 'none') return 'playlist';

    if (currentMode === 'playlist') return 'title';
    
    return 'none';
};

export const updatePageTitle = (isPlaying, track) => {
    if (!isPlaying) return 'Spotify';

    const { first_name, last_name, title } = track;

    const authorName = `${capitalize(first_name)} ${capitalize(last_name)}`;

    return `${title} • ${authorName}`;
};
