import SidebarHeader from './SidebarHeader';
import SidebarMain from './SidebarMain';
import SidebarFooter from './SidebarFooter';

import styles from './Sidebar.module.css';

const Sidebar = () => {
    return (
        <aside className={styles.sidebar}>
            <SidebarHeader />
            <SidebarMain />
            <SidebarFooter />
        </aside>
    );
};

export default Sidebar;