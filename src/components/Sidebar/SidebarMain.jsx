import { useState, useEffect } from 'react';

import SidebarLink from './SidebarLink';
import ModalSidebar from '../Modal/ModalSidebar';

import { ReactComponent as LogoHomeFill } from './../../assets/icons/home-fill.svg';
import { ReactComponent as LogoHomeLine } from './../../assets/icons/home-line.svg';
import { ReactComponent as LogoSearchFill } from './../../assets/icons/search-fill.svg';
import { ReactComponent as LogoSearchLine } from './../../assets/icons/search-line.svg';
import { ReactComponent as LogoLibraryFill } from './../../assets/icons/library-fill.svg';
import { ReactComponent as LogoLibraryLine } from './../../assets/icons/library-line.svg';
import { ReactComponent as LogoLikeFill } from './../../assets/icons/like-fill.svg';
import { ReactComponent as LogoPlaylistCreate } from './../../assets/icons/playlist-create.svg';

import styles from './SidebarMain.module.css';

const SidebarMain = () => {
    const [isPlayListModalOpen, setIsPlaylistModalOpen] = useState(false);
    const [isLikedModalOpen, setIsLikedModalOpen] = useState(false);

    const togglePlaylistModal = () => {
        if (isPlayListModalOpen) return setIsPlaylistModalOpen(false);

        return setIsPlaylistModalOpen(true);
    };

    const toggleLikedModal = () => {
        if (isLikedModalOpen) return setIsLikedModalOpen(false);

        return setIsLikedModalOpen(true);
    };

    const modalPlaylistContent = {
        title: 'Créer une playlist',
        description: 'Connectez-vous pour créer et partager des playlists.',
        button: 'Plus tard'
    };

    const modalLikedContent = {
        title: 'Profitez de vos titres likés',
        description: 'Bientôt disponible...',
        button: `D'accord`
    };

    return (
        <section className={styles.main}>
            <section className={styles.commonLinks}>
                <SidebarLink 
                    to="/" 
                    text="Accueil" 
                    logoFill={<LogoHomeFill />} 
                    logoLine={<LogoHomeLine />} 
                    activeClass={styles.active} 
                />

                <SidebarLink 
                    to="/search" 
                    text="Rechercher" 
                    logoFill={<LogoSearchFill />} 
                    logoLine={<LogoSearchLine />} 
                    activeClass={styles.active} 
                />

                <SidebarLink 
                    to="/collection" 
                    text="Bibliothèque" 
                    logoFill={<LogoLibraryFill />} 
                    logoLine={<LogoLibraryLine />} 
                    activeClass={styles.active} 
                />
            </section>

            <section className={styles.userLinks}>
                <div className={styles.btnContainer}>
                    <button className={styles.playlistLink} onClick={togglePlaylistModal}>
                        <div>
                            <LogoPlaylistCreate />
                        </div>
                        <span className={styles.buttonText}>Créer une playlist</span>
                    </button>
                    <ModalSidebar
                        key={0}
                        isOpen={isPlayListModalOpen} 
                        setIsOpen={togglePlaylistModal}
                        closeModal={setIsPlaylistModalOpen}
                        content={modalPlaylistContent}
                    />
                </div>

                <div className={styles.btnContainer}>
                    <button className={styles.likeLink} onClick={toggleLikedModal}>
                        <div>
                            <LogoLikeFill />
                        </div>
                        <span className={styles.buttonText}>Titres likés</span>
                    </button>
                    <ModalSidebar
                        key={1} 
                        isOpen={isLikedModalOpen} 
                        setIsOpen={toggleLikedModal}
                        closeModal={setIsLikedModalOpen}
                        content={modalLikedContent}
                    />
                </div>
            </section>
        </section>
    );
};

export default SidebarMain;