import { Link } from 'react-router-dom';

import { ReactComponent as LogoCompany } from './../../assets/images/spotify-logo-full.svg';

import styles from './SidebarHeader.module.css';

const SidebarHeader = () => {
    return (
        <header>
            <Link to="/">
                <LogoCompany title="Spotify" className={styles.logo} />
            </Link>
        </header>
    );
};

export default SidebarHeader;