import { NavLink } from 'react-router-dom';

const SidebarLink = (props) => {
    const { to, text, logoFill, logoLine, activeClass } = props;

    const onClassActive = ({ isActive }) => isActive ? activeClass : '';

    const renderLinkContent = ({ isActive }) => {
        const logo = isActive ? logoFill : logoLine;

        return (
            <>
                {logo}
                <span>{text}</span>
            </>
        )
    };

    return (
        <NavLink to={to} className={onClassActive}>
            {renderLinkContent}
        </NavLink>
    );
};

export default SidebarLink;