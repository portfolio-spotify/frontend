import styles from './TrackImage.module.css';

const TrackImage = (props) => {
    const { src } = props;

    return ( <img src={src} alt="cover title" className={styles.cover} /> );
};

export default TrackImage;
