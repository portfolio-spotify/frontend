import styles from './Track.module.css';

const Track = (props) => {
    const { isTrackPlaying } = props;

    const itemStyle = isTrackPlaying ? `${styles.container} ${styles.playing}` : `${styles.container}`;

    return (
        <li className={itemStyle}>
            {props.children}
        </li>
    );
};

export default Track;