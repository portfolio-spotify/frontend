import styles from './TrackNumber.module.css';

import { ReactComponent as PlayIcon } from '../../../assets/icons/play.svg';
import { ReactComponent as PauseIcon } from '../../../assets/icons/pause.svg';
import playgif from '../../../assets/gifs/equaliser.f5eb96f2.gif';

const TrackNumber = (props) => {
    const { isTrackPlaying, onPlayPause } = props;

    return (
        <div className={styles.count}>
            <span className={`number ${styles.number}`}>
                {props.number}
            </span>
            <button className={`logo-play ${styles.playPauseIcon}`} onClick={onPlayPause}>
                { isTrackPlaying && <img className={`track-gif ${styles.gif}`} src={playgif} alt='playing gif' /> }
                { isTrackPlaying ? <PauseIcon /> : <PlayIcon /> }
            </button>
        </div>
    );
};

export default TrackNumber;