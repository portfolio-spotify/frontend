import styles from './TrackDuration.module.css';

import { ReactComponent as LogoMenuBold } from './../../../assets/icons/menu-bold.svg';

const TrackDuration = (props) => {
    return (
        <div className={styles.duration}>
            <span className={styles.length}>
                {props.duration}
            </span>
            <LogoMenuBold className={`track-menu ${styles.menu}`} />
        </div>
    );
};

export default TrackDuration;