import { useState, useEffect } from 'react';
import capitalize from 'lodash/capitalize';

import useAudioPlayerStore from '../../stores/useAudioPlayerStore';

import Track from './Track';
import TrackNumber from './TrackNumber/TrackNumber';
import TrackDetails from './TrackDetails/TrackDetails';
import TrackRelease from './TrackRelease/TrackRelease';
import TrackListened from './TrackListened/TrackListened';
import TrackDuration from './TrackDuration/TrackDuration';
import TrackLiked from './TrackLiked';

import { formatDate, formatDuration } from '../../utils/date';

const TrackAlbum = (props) => {
    const { track, album, albumTracks, user, currentTrackIndex } = props;

    const isPlaying = useAudioPlayerStore((state) => state.isPlaying);
    const tracks = useAudioPlayerStore((state) => state.tracks);
    const trackIndex = useAudioPlayerStore((state) => state.trackIndex);
    const setIsPlaying = useAudioPlayerStore((state) => state.setIsPlaying);
    const setAlbum = useAudioPlayerStore((state) => state.setAlbum);
    const setTracks = useAudioPlayerStore((state) => state.setTracks);
    const setAuthor = useAudioPlayerStore((state) => state.setAuthor);
    const setTrackIndex = useAudioPlayerStore((state) => state.setTrackIndex);
    const [isTrackPlaying, setIsTrackPlaying] = useState(false);

    /* Update when current track playings */
    useEffect(() => {
        if (isPlaying) {
            const isCurrentTrackPlaying = track.id === tracks[trackIndex].id;

            if (isCurrentTrackPlaying) return setIsTrackPlaying(true);
        }

        setIsTrackPlaying(false);
    }, [isPlaying, track.id, trackIndex, tracks]);
    
    const togglePlayPause = () => {
        const isCurrentTrackPlaying = track.id === tracks[trackIndex].id;

        if (isPlaying && isCurrentTrackPlaying) return setIsPlaying(false);

        if (!isPlaying && isCurrentTrackPlaying) return setIsPlaying(true);

        setAlbum(album);
        setTracks(albumTracks);
        setAuthor(user);
        setIsPlaying(true);
        setTrackIndex(currentTrackIndex);
    };

    const authorName = `${capitalize(track.first_name)} ${capitalize(track.last_name)}`;
    const releaseDate = formatDate(track.created_at);
    const duration = formatDuration(track.duration);

    return (
        <Track isTrackPlaying={isTrackPlaying}>
            <TrackNumber 
                number={track.order_number} 
                isTrackPlaying={isTrackPlaying} 
                onPlayPause={togglePlayPause} 
            />
            <TrackDetails title={track.title} artist={authorName} />
            <TrackRelease date={releaseDate} />
            <TrackListened listenedTotal={track.play_count} />
            <TrackLiked track={track} />
            <TrackDuration duration={duration} />
        </Track>
    );
};

export default TrackAlbum;