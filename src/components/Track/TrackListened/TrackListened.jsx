import styles from './TrackListened.module.css';

const TrackListened = (props) => {
    return (
        <span className={styles.listened}>
            {props.listenedTotal}
        </span>
    );
};

export default TrackListened;