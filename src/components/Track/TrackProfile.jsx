import Track from './Track';
import TrackNumber from './TrackNumber/TrackNumber';
import TrackDetails from './TrackDetails/TrackDetails';
import TrackDuration from './TrackDuration/TrackDuration';
import TrackListened from './TrackListened/TrackListened';
import TrackLiked from './TrackLiked';
import TrackImage from './TrackImage';

import { formatDuration } from '../../utils/date';

const TrackProfile = (props) => {
    const { track, trackNumber } = props;

    const duration = formatDuration(track.duration);

    return (
        <Track>
            <TrackNumber number={trackNumber}/>
            <TrackImage src={track.imageURL} />
            <TrackDetails title={track.title} />
            <TrackListened listenedTotal={track.play_count} />
            <TrackLiked track={track} />
            <TrackDuration duration={duration} />
        </Track>
    );
};

export default TrackProfile;

/* 
    <img src={singleImg} alt="cover title" className={styles.singleCover} />

*/

    