import styles from './TrackRelease.module.css';

const TrackRelease = (props) => {
    return (
        <span className={styles.release}>
            {props.date}
        </span>
    );
};

export default TrackRelease;