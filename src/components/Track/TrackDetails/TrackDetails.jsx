import styles from './TrackDetails.module.css';

const TrackDetails = (props) => {
    const { title, artist } = props;

    const artistElement = (
        artist && 
        <span className={`artist-name ${styles.trackArtist}`}>
            {artist}
        </span>
    );
    
    return (
        <div className={styles.trackDetails}>
            <span className={'track-title'}>
                {title}
            </span>
            {artistElement}
        </div>
    );
};

export default TrackDetails;