import { useEffect, useState } from 'react';

import useAudioPlayerStore from '../../stores/useAudioPlayerStore';

import styles from './TrackLiked.module.css';

import { ReactComponent as LikeIconLine } from '../../assets/icons/like-line.svg';
import { ReactComponent as LikeIconFill } from '../../assets/icons/like-fill.svg';

const TrackLiked = (props) => {
    const { track } = props;

    const likedAudios = useAudioPlayerStore((state) => state.likedAudios);
    const setLikedAudios = useAudioPlayerStore((state) => state.setLikedAudios);
    const [isLiked, setIsLiked] = useState(false);

    /* Update current track like status */
    useEffect(() => {
        if (likedAudios) {
            const savedLikes = new Set(likedAudios);

            const isTrackLiked = savedLikes.has(track.id);

            if (isTrackLiked) return setIsLiked(true);

            return setIsLiked(false);
        }

        setIsLiked(false);
    }, [likedAudios, track.id]);

    const onLike = () => {
        if (isLiked) {
            const savedLikes = new Set(likedAudios);
            savedLikes.delete(track.id);
            setLikedAudios(savedLikes);
            return setIsLiked(false);
        }

        const savedLikes = new Set(likedAudios);
        savedLikes.add(track.id);
        setLikedAudios(savedLikes);
        return setIsLiked(true);
    };

    const likeIconStyle = isLiked ? `${styles.like} ${styles.active}` : `${styles.like}`;

    return (
        <div className={styles.container}>
            <button className={`track-like ${likeIconStyle}`} onClick={onLike}>
                { isLiked ? <LikeIconFill /> : <LikeIconLine /> }
            </button>
        </div>
        
    );
};

export default TrackLiked;
