import styles from './ButtonPrimary.module.css';

import { ReactComponent as PlayIcon } from './../../assets/icons/play.svg';
import { ReactComponent as PauseIcon } from './../../assets/icons/pause.svg';

const ButtonPrimary = (props) => {
    const { size, isPlaying, onClick } = props;

    return (
        <button className={`${styles.btn} ${styles[size]}`} onClick={onClick}>
            { isPlaying ? <PauseIcon /> : <PlayIcon /> }
        </button>
    );
};

export default ButtonPrimary;