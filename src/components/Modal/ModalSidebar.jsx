import { useEffect, useRef } from 'react';

import styles from './ModalSidebar.module.css';

const ModalSidebar = (props) => {
    const { isOpen, setIsOpen, closeModal, content } = props;

    const ref = useRef(null);

    const handlesClickOutside = (event) => {
        if (ref.current && !ref.current.contains(event.target)) {
            closeModal(false);
        }
    };

    useEffect(() => {
        document.addEventListener('click', handlesClickOutside, true);

        return () => {
            document.removeEventListener('click', handlesClickOutside, true);
        };
    }, []);

    const modalStyle = isOpen ? `${styles.modal} ${styles.active}` : `${styles.modal}`;

    return (
        <section ref={ref} className={modalStyle}>
            <span className={styles.modalTitle}>{content.title}</span>
            <p className={styles.text}>{content.description}</p>
            <span className={styles.btn} onClick={setIsOpen}>{content.button}</span>
        </section>
    );
};

export default ModalSidebar;
