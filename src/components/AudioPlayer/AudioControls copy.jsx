import { useRef, useEffect, forwardRef } from 'react';
import useUpdateEffect from 'react-use/lib/useUpdateEffect'
import { Helmet } from 'react-helmet-async';
import capitalize from 'lodash/capitalize';

import useAudioPlayerStore from '../../stores/useAudioPlayerStore';
import { formatDuration } from '../../utils/date';

import styles from './AudioControls.module.css';

import { ReactComponent as RandomIcon } from '../../assets/icons/random.svg';
import { ReactComponent as PreviousIcon } from '../../assets/icons/previous.svg';
import { ReactComponent as NextIcon } from '../../assets/icons/next.svg';
import { ReactComponent as PlayIcon } from '../../assets/icons/play.svg';
import { ReactComponent as PauseIcon } from '../../assets/icons/pause.svg';
import { ReactComponent as RepeatIcon } from '../../assets/icons/repeat.svg';
import { ReactComponent as Repeat1xIcon } from '../../assets/icons/repeat-x1.svg';

const MINIMUN_SECONDS_TO_REPEAT = 3;

// 1. Gérer les actions random
// 2. Ajouter les controles full screen
// 3. Ajouter action like + animation
// 4. Ajouter sauvegarde dans localStorage au moment de quitter la page
// 5. Tester audioRef.current.loop = true pour boucler sur l'élément
//      - si repeating = title : loop = true;
// 6. Refactor composant progressBar
// 7. Refactor composant Button avec Icon dedans

// si random : récupérer random index
//      - next : random
//      - prev : conserver historique des tracks précédentes
// si pas repeat : une fois arrivé à la fin, arrêter de jouer





// RANDOM :
// - réorganiser la playlist de manière random dès clique sur random : 
//          - shuffle les audios suivants
//          - conserver l'audio en cours

// stocker un tableau avec des indexes représentants la file d'attente de la playlist
// index = 1;
// playlist = [0, 1, 2, 3, 4];
// waitingList = [];
// si random : waitingList = [3, 1, 4, 0, 2];
//      - conserver trace de index pour pouvoir rejouer à partir de cet index

// si random : shuffle tous les audios sauf celui en cours
//      - créer random playlist avec tous les audios sauf celui en cours
//      - 
// si !random : reprendre à index correspondant de la playlist initiale
// 


const updatePageTitle = (isPlaying, track) => {
    if (!isPlaying) return 'Spotify';

    const { first_name, last_name, title } = track;

    const authorName = `${capitalize(first_name)} ${capitalize(last_name)}`;

    return `${title} • ${authorName}`;
};

const AudioControls = forwardRef((props, ref) => {
    const { track } = props;
    const audioRef = ref;

    const isPlaying = useAudioPlayerStore((state) => state.isPlaying);
    const isRandom = useAudioPlayerStore((state) => state.isRandom);
    const repeating = useAudioPlayerStore((state) => state.repeating);
    const tracks = useAudioPlayerStore((state) => state.tracks);
    const trackIndex = useAudioPlayerStore((state) => state.trackIndex);
    const trackProgress = useAudioPlayerStore((state) => state.trackProgress);
    const setIsPlaying = useAudioPlayerStore((state) => state.setIsPlaying);
    const setIsRandom = useAudioPlayerStore((state) => state.setIsRandom);
    const setRepeating = useAudioPlayerStore((state) => state.setRepeating);
    const setTrackIndex = useAudioPlayerStore((state) => state.setTrackIndex);
    const setTrackProgress = useAudioPlayerStore((state) => state.setTrackProgress);

    const intervalRef = useRef();
    const repeatingRef = useRef(repeating);
    const animationRef = useRef();

    const { audioFileURL, duration, } = track;

    const pageTitle = updatePageTitle(isPlaying, track);

    const currentTrackProgressPercentage = (trackProgress / duration) * 100;

    const handlesNextTrack = () => {
        if (trackIndex < tracks.length - 1) return setTrackIndex(trackIndex + 1);

        setTrackIndex(0);

        if (repeating === 'none') setIsPlaying(false);
    };
    
    const onPrevClick = () => {
        if (trackProgress >= MINIMUN_SECONDS_TO_REPEAT) {
            audioRef.current.currentTime = 0;
            setTrackProgress(audioRef.current.currentTime);
            return;
        }

        if (trackIndex - 1 < 0) return setTrackIndex(tracks.length - 1);

        return setTrackIndex(trackIndex - 1);
    };

    const onNextClick = () => {
        if (repeating === 'title') setRepeating('playlist');

        handlesNextTrack();
    };

    const onTrackEnd = () => {
        if (repeatingRef.current === 'title') {
            setTrackProgress(audioRef.current.currentTime);

            audioRef.current.play();

            startTimer();

            return;
        }

        handlesNextTrack();
    };

    const onRandomClick = () => setIsRandom();
    
    const onRepeatClick = () => setRepeating();

    const startTimer = () => {
        clearInterval(intervalRef.current);

        const interval = setInterval(() => {
            if (audioRef.current.ended) onTrackEnd();
            
            else setTrackProgress(audioRef.current.currentTime);
        }, 1_000);

        intervalRef.current = interval;
    };

    const whilePlaying = () => {
        setTrackProgress(audioRef.current.currentTime);
        animationRef.current = requestAnimationFrame(whilePlaying);
    };

    const togglePlayPause = () => {
        const nextIsPlaying = !isPlaying;

        setIsPlaying(nextIsPlaying);

        if (nextIsPlaying) {
            audioRef.current.play();
            // startTimer();
            animationRef.current = requestAnimationFrame(whilePlaying);
            return;
        }

        cancelAnimationFrame(animationRef.current);
        // clearInterval(intervalRef.current);
        audioRef.current.pause();
    };

    /* Handles changing audio */
    useUpdateEffect(() => {
        audioRef.current.pause();

        audioRef.current = new Audio(audioFileURL);

        setTrackProgress(audioRef.current.currentTime);

        if (isPlaying) {
            audioRef.current.play();
            // startTimer();
        }
    }, [trackIndex, audioFileURL]);

    /* Pause & cleanup interval on unmount */
    useEffect(() => {
        const interval = intervalRef.current;

        return () => {
            audioRef.current.pause();
            clearInterval(interval);
        };
    }, []);

    const onScrub = (value) => {
        // clearInterval(intervalRef.current);
        cancelAnimationFrame(animationRef.current);

        setTrackProgress(value);
    };
    
    const onScrubEnd = () => {
        // if not already playing : start
        // if (!isPlaying) setIsPlaying(true);
        audioRef.current.currentTime = trackProgress;
        animationRef.current = requestAnimationFrame(whilePlaying);
        // startTimer();
    };

    const repeatIcon = repeating === 'title' ? <Repeat1xIcon /> : <RepeatIcon />;
    const repeatIconStyle = repeating !== 'none' ? `${styles.repeat} ${styles.active}` : `${styles.repeat}`;
    const randomIconStyle = isRandom ? `${styles.random} ${styles.active}` : `${styles.repeat}`;

    const PlayPauseButton = (
        <button type="button" className={styles.play} onClick={togglePlayPause}>
            {isPlaying ? <PauseIcon /> : <PlayIcon />}
        </button>
    );

    return (
        <>
            <Helmet defaultTitle={pageTitle} />
            <div className={styles.player}>
                <div className={styles.audioControls}>
                    <button type="button" className={randomIconStyle} onClick={onRandomClick}>
                        <RandomIcon />
                    </button>
                    <button type="button" className={styles.previous} onClick={onPrevClick}>
                        <PreviousIcon />
                    </button>
                    {PlayPauseButton}
                    <button type="button" className={styles.next} onClick={onNextClick}>
                        <NextIcon />
                    </button>
                    <button type="button" className={repeatIconStyle} onClick={onRepeatClick}>
                        {repeatIcon}
                    </button>
                </div>
                <div className={styles.audioState}>
                    <time>{formatDuration(trackProgress)}</time>
                    <input 
                        type="range" 
                        value={trackProgress} 
                        min="0" 
                        max={duration} 
                        step="1" 
                        onChange={(e) => onScrub(e.target.value)}
                        onMouseUp={onScrubEnd}
                        onKeyUp={onScrubEnd}
                        className={styles.audioProgressBar}
                        style={{ "--currentPercentage": `${currentTrackProgressPercentage}%` }}
                    />
                    <time>{formatDuration(parseFloat(duration))}</time>
                </div>
            </div>
        </>
    );
});

export default AudioControls;
