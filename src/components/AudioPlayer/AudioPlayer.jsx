import { useRef, useEffect } from 'react';
import useAudioPlayerStore from '../../stores/useAudioPlayerStore';

import styles from './AudioPlayer.module.css';

import AudioDetails from './AudioDetails';
import AudioControls from './AudioControls';
import AudioSound from './AudioSound';

const AudioPlayer = () => {
    const tracks = useAudioPlayerStore((state) => state.tracks);
    const author = useAudioPlayerStore((state) => state.author);
    const album = useAudioPlayerStore((state) => state.album);
    const trackIndex = useAudioPlayerStore((state) => state.trackIndex);

    const audio = tracks.length && new Audio(tracks[trackIndex].audioFileURL);

    const audioRef = useRef(audio);

    const onPageLeave = () => {
        localStorage.setItem('prevTracks', JSON.stringify(tracks));
        localStorage.setItem('prevAuthor', JSON.stringify(author));
        localStorage.setItem('prevAlbum', JSON.stringify(album));
        localStorage.setItem('prevTrackIndex', JSON.stringify(trackIndex));
    };
    
    useEffect(() => {
        if (tracks.length) {
            return () => window.removeEventListener('beforeunload', onPageLeave);
        }
    });
    
    if (!tracks.length) return;
    
    window.addEventListener('beforeunload', onPageLeave);

    const track = tracks[trackIndex];

    return (
        <section className={styles.audioPlayer}>
            <AudioDetails track={track} album={album} author={author} />

            <AudioControls ref={audioRef} track={track} />

            <AudioSound ref={audioRef} />
        </section>
    );
};

export default AudioPlayer;
