import { useState, useEffect } from 'react';
import { NavLink } from 'react-router-dom';
import capitalize from 'lodash/capitalize';

import useAudioPlayerStore from '../../stores/useAudioPlayerStore';

import styles from './AudioDetails.module.css';
import { ReactComponent as LikeLineIcon } from '../../assets/icons/like-line.svg';
import { ReactComponent as LikeFillIcon } from '../../assets/icons/like-fill.svg';

const AudioDetails = (props) => {
    const { album, author, track } = props;

    const likedAudios = useAudioPlayerStore((state) => state.likedAudios);
    const setLikedAudios = useAudioPlayerStore((state) => state.setLikedAudios);
    const [isLiked, setIsLiked] = useState(false);

    const albumURL = `/album/${album.url}`;
    const imageAlbum = album.imageURL;
    const trackURL = `/track/${track.url}`;
    const trackTitle = track.title;
    const authorURL = `/artist/${author.url}`;
    const authorName = `${capitalize(author.first_name)} ${capitalize(author.last_name)}`;

    /* Update current track like status */
    useEffect(() => {
        if (likedAudios) {
            const savedLikes = new Set(likedAudios);
            
            const isTrackLiked = savedLikes.has(track.id);

            if (isTrackLiked) return setIsLiked(true);

            return setIsLiked(false);
        }

        setIsLiked(false);
    }, [likedAudios, track.id]);

    const onLike = () => {
        if (isLiked) {
            const savedLikes = new Set(likedAudios);
            savedLikes.delete(track.id);
            setLikedAudios(savedLikes);
            return setIsLiked(false);
        }

        const savedLikes = new Set(likedAudios);
        savedLikes.add(track.id);
        setLikedAudios(savedLikes);
        return setIsLiked(true);
    };

    const likeIconStyle = isLiked ? `${styles.like} ${styles.active}` : `${styles.like}`;

    return (
        <div className={styles.track}>
            <NavLink to={albumURL}>
                <img src={imageAlbum} alt="audio cover" />
            </NavLink>
            <div className={styles.details}>
                <NavLink to={trackURL} className={styles.title}>{trackTitle}</NavLink>
                <NavLink to={authorURL} className={styles.artist}>{authorName}</NavLink>
            </div>
            <button className={likeIconStyle} onClick={onLike}>
                {isLiked ? <LikeFillIcon /> : <LikeLineIcon />}
            </button>
        </div>
    );
};

export default AudioDetails;