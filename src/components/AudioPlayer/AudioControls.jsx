import { useRef, useEffect, forwardRef } from 'react';
import useUpdateEffect from 'react-use/lib/useUpdateEffect'
import { Helmet } from 'react-helmet-async';
import shuffle from 'lodash/shuffle';

import useAudioPlayerStore from '../../stores/useAudioPlayerStore';
import { formatDuration } from '../../utils/date';
import { getAudioRepeatMode, updatePageTitle } from '../../utils/audio';

import styles from './AudioControls.module.css';

import { ReactComponent as RandomIcon } from '../../assets/icons/random.svg';
import { ReactComponent as PreviousIcon } from '../../assets/icons/previous.svg';
import { ReactComponent as NextIcon } from '../../assets/icons/next.svg';
import { ReactComponent as PlayIcon } from '../../assets/icons/play.svg';
import { ReactComponent as PauseIcon } from '../../assets/icons/pause.svg';
import { ReactComponent as RepeatIcon } from '../../assets/icons/repeat.svg';
import { ReactComponent as Repeat1xIcon } from '../../assets/icons/repeat-x1.svg';

const MINIMUN_SECONDS_TO_REPEAT = 3;

const AudioControls = forwardRef((props, ref) => {
    const { track } = props;

    const isPlaying = useAudioPlayerStore((state) => state.isPlaying);
    const isRandom = useAudioPlayerStore((state) => state.isRandom);
    const repeating = useAudioPlayerStore((state) => state.repeating);
    const tracks = useAudioPlayerStore((state) => state.tracks);
    const trackIndex = useAudioPlayerStore((state) => state.trackIndex);
    const trackProgress = useAudioPlayerStore((state) => state.trackProgress);
    const setIsPlaying = useAudioPlayerStore((state) => state.setIsPlaying);
    const setIsRandom = useAudioPlayerStore((state) => state.setIsRandom);
    const setRepeating = useAudioPlayerStore((state) => state.setRepeating);
    const setTrackIndex = useAudioPlayerStore((state) => state.setTrackIndex);
    const setTrackProgress = useAudioPlayerStore((state) => state.setTrackProgress);
    const setTracks = useAudioPlayerStore((state) => state.setTracks);

    const audioRef = ref;
    const repeatingRef = useRef(repeating);
    const animationRef = useRef();

    const { audioFileURL, duration, } = track;

    const pageTitle = updatePageTitle(isPlaying, track);
    const currentTrackProgressPercentage = (trackProgress / duration) * 100;

    useEffect(() => {
        audioRef.current.currentTime = trackProgress;
    }, []);

    const onPageLeave = () => {
        localStorage.setItem('wasRandom', JSON.stringify(isRandom));
        localStorage.setItem('wasRepeating', JSON.stringify(repeating));
        localStorage.setItem('prevTrackProgress', JSON.stringify(trackProgress));
    };

    window.addEventListener('beforeunload', onPageLeave);
    useEffect(() => () => window.removeEventListener('beforeunload', onPageLeave));

    const handlesNextTrack = () => {
        if (trackIndex < tracks.length - 1) return setTrackIndex(trackIndex + 1);

        setTrackIndex(0);

        if (repeatingRef.current === 'none') setIsPlaying(false);
    };
    
    const onPrevClick = () => {
        if (trackProgress >= MINIMUN_SECONDS_TO_REPEAT) {
            audioRef.current.currentTime = 0;
            setTrackProgress(audioRef.current.currentTime);
            return;
        }

        if (trackIndex - 1 < 0) return setTrackIndex(tracks.length - 1);

        return setTrackIndex(trackIndex - 1);
    };

    const onNextClick = () => {
        cancelAnimationFrame(animationRef.current);

        if (repeatingRef.current === 'title') {
            setRepeating('playlist');
            repeatingRef.current = 'playlist';
        }

        handlesNextTrack();
    };

    const onTrackEnd = () => {
        cancelAnimationFrame(animationRef.current);

        if (repeatingRef.current === 'title') return;

        handlesNextTrack();
    };

    const onRandomClick = () => {
        const nextIsRandom = !isRandom;

        setIsRandom(nextIsRandom);

        // if (nextIsRandom) {
        if (false) {
            // SHUFFLE
            // retirer track avec index = currentIndex
            const withoutCurrentTrack = tracks.filter((track, index) => index !== trackIndex);
            console.log('CURRENT TRACK', track)
            console.log('WITHOUT CURRENT', withoutCurrentTrack)

            // shuffle le tableau restant
            const shuffledTracks = shuffle(withoutCurrentTrack);
            console.log('SHUFFLED', shuffledTracks)

            // ajouter la current track au début du tableau
            const withCurrentTrack = [track].concat(shuffledTracks);
            console.log('WITH CURRENT TRACK', withCurrentTrack)

            // ajouter les nouvelles tracks
            setTracks(withCurrentTrack);

            // changer currentIndex = 0
            // setTrackIndex(0);
        }

        // BASE

    };

    const onRepeatClick = () => {
        const nextMode = getAudioRepeatMode(repeating);

        setRepeating(nextMode);
        repeatingRef.current = nextMode;
        audioRef.current.loop = (nextMode === 'title');
    };

    const whilePlaying = () => {
        if (audioRef.current.ended) return onTrackEnd();

        setTrackProgress(audioRef.current.currentTime);
        animationRef.current = requestAnimationFrame(whilePlaying);
    };

    const togglePlayPause = () => {
        const nextIsPlaying = !isPlaying;

        setIsPlaying(nextIsPlaying);

        if (nextIsPlaying) {
            audioRef.current.play();
            animationRef.current = requestAnimationFrame(whilePlaying);
            return;
        }

        cancelAnimationFrame(animationRef.current);
        audioRef.current.pause();
    };

    /* Handles changing audio */
    useUpdateEffect(() => {
        audioRef.current.pause();

        audioRef.current = new Audio(audioFileURL);

        setTrackProgress(audioRef.current.currentTime);

        if (isPlaying) {
            audioRef.current.play();
            animationRef.current = requestAnimationFrame(whilePlaying);
            return;
        }
    }, [trackIndex, audioFileURL]);

    /* Pause & cleanup interval on unmount */
    useEffect(() => {
        const animation = animationRef.current;

        return () => {
            audioRef.current.pause();
            cancelAnimationFrame(animation);
        };
    }, [audioRef]);

    useEffect(() => {
        if (isPlaying) {
            audioRef.current.play();
            animationRef.current = requestAnimationFrame(whilePlaying);
            return;
        }

        cancelAnimationFrame(animationRef.current);
        audioRef.current.pause();
    }, [isPlaying, audioRef]);

    const onScrub = (value) => {
        cancelAnimationFrame(animationRef.current);
        setTrackProgress(value);
    };
    
    const onScrubEnd = () => {
        audioRef.current.currentTime = trackProgress;
        animationRef.current = requestAnimationFrame(whilePlaying);
    };

    const repeatIconStyle = repeating !== 'none' ? `${styles.repeat} ${styles.active}` : `${styles.repeat}`;
    const randomIconStyle = isRandom ? `${styles.random} ${styles.active}` : `${styles.random}`;

    return (
        <>
            <Helmet defaultTitle={pageTitle} />
            <div className={styles.player}>
                <div className={styles.audioControls}>
                    <button type="button" className={randomIconStyle} onClick={onRandomClick}>
                        <RandomIcon />
                    </button>
                    <button type="button" className={styles.previous} onClick={onPrevClick}>
                        <PreviousIcon />
                    </button>
                    <button type="button" className={styles.play} onClick={togglePlayPause}>
                        {isPlaying ? <PauseIcon /> : <PlayIcon />}
                    </button>
                    <button type="button" className={styles.next} onClick={onNextClick}>
                        <NextIcon />
                    </button>
                    <button type="button" className={repeatIconStyle} onClick={onRepeatClick}>
                        {repeating === 'title' ? <Repeat1xIcon /> : <RepeatIcon />}
                    </button>
                </div>
                <div className={styles.audioState}>
                    <time>{formatDuration(trackProgress)}</time>
                    <input 
                        type="range" 
                        value={trackProgress} 
                        min="0" 
                        max={duration}
                        step="1" 
                        onChange={(e) => onScrub(e.target.value)}
                        onMouseUp={onScrubEnd}
                        onKeyUp={onScrubEnd}
                        className={styles.audioProgressBar}
                        style={{ "--currentPercentage": `${currentTrackProgressPercentage}%` }}
                    />
                    <time>{formatDuration(parseFloat(duration))}</time>
                </div>
            </div>
        </>
    );
});

export default AudioControls;
