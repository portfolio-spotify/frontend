import { forwardRef, useRef, useState, useEffect } from 'react';
import useAudioPlayerStore from '../../stores/useAudioPlayerStore';

import styles from './AudioSound.module.css';

import { ReactComponent as SoundHighIcon } from '../../assets/icons/sound-high.svg';
import { ReactComponent as SoundMediumIcon } from '../../assets/icons/sound-medium.svg';
import { ReactComponent as SoundLowIcon } from '../../assets/icons/sound-low.svg';
import { ReactComponent as SoundMuteIcon } from '../../assets/icons/sound-mute.svg';
import { ReactComponent as FullScreenIcon } from '../../assets/icons/fullscreen.svg';
import { ReactComponent as FullScreenOutIcon } from '../../assets/icons/fullscreen-out.svg';

const AudioSound = forwardRef((props, ref) => {
    
    const soundProgress = useAudioPlayerStore((state) => state.soundProgress);
    const isMuted = useAudioPlayerStore((state) => state.isMuted);
    const trackMutedFrom = useAudioPlayerStore((state) => state.trackMutedFrom);
    const setSoundProgress = useAudioPlayerStore((state) => state.setSoundProgress);
    const setIsMuted = useAudioPlayerStore((state) => state.setIsMuted);
    const setTrackMutedFrom = useAudioPlayerStore((state) => state.setTrackMutedFrom);
    const [isFullScreen, setIsFullScreen] = useState(false);

    const audioRef = ref;
    const fullscreenBtnRef = useRef();
    
    useEffect(() => {
        audioRef.current.volume = soundProgress;
    });

    const currentSoundPercentage = soundProgress * 100;

    const onPageLeave = () => {
        localStorage.setItem('prevSoundProgress', JSON.stringify(soundProgress));
        localStorage.setItem('wasMuted', JSON.stringify(isMuted));
        localStorage.setItem('prevTrackMutedFrom', JSON.stringify(trackMutedFrom));
    };

    window.addEventListener('beforeunload', onPageLeave);
    useEffect(() => () => window.removeEventListener('beforeunload', onPageLeave));

    const onScrub = (value) => {
        const soundLevel = parseFloat(value);

        setSoundProgress(soundLevel);

        audioRef.current.volume = value;

        if (soundLevel === 0) {
            setIsMuted(true);
            return setTrackMutedFrom(1);
        }

        return setIsMuted(false);
    };

    const soundIcon = (
        soundProgress === 0 ? <SoundMuteIcon /> :
        soundProgress <= 0.33 ? <SoundLowIcon /> :
        soundProgress <= 0.66 ? <SoundMediumIcon /> :
        <SoundHighIcon /> 
    );

    const onMuteClick = () => {
        if (isMuted) {
            setIsMuted(false);
            setSoundProgress(trackMutedFrom);
            return setTrackMutedFrom(1);
        }

        setIsMuted(true);
        setTrackMutedFrom(soundProgress);
        setSoundProgress(0);
    };

    const toggleFullScreen = () => {
        // const nextIsFullScreen = !isFullScreen;
        // const btn = fullscreenBtnRef.current;

        // setIsFullScreen(nextIsFullScreen);

        // if (!nextIsFullScreen) {
        //     if (btn.exitFullscreen)
        //         btn.exitFullscreen();
        //     else if (btn.webkitExitFullscreen)
        //         btn.webkitExitFullscreen();
        //     else if (btn.msExitFullscreen)
        //         btn.msExitFullscreen();

        //     return;
        // }

        // if (btn.requestFullscreen)
        //     btn.requestFullscreen();
        // else if (btn.webkitRequestFullscreen)
        //     btn.webkitRequestFullscreen();
        // else if (btn.msRequestFullscreen)
        //     btn.msRequestFullscreen();
    };

    return (
        <div className={styles.actions}>
            <button className={styles.sound} onClick={onMuteClick}>
                {soundIcon}
            </button>

            <input 
                type="range" 
                value={soundProgress} 
                min="0" 
                max="1"
                step="0.01"
                onChange={(e) => onScrub(e.target.value)}
                className={styles.audioProgressBar}
                style={{ "--currentPercentage": `${currentSoundPercentage}%` }}
            />

            <button ref={fullscreenBtnRef} className={styles.fullscreen} onClick={toggleFullScreen}>
                {isFullScreen ? <FullScreenOutIcon /> : <FullScreenIcon />}
            </button>
        </div>
    );
});

export default AudioSound;