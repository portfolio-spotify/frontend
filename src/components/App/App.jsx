import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { Helmet } from 'react-helmet-async';

import LayoutWithSidebar from '../../layouts/LayoutWithSidebar';
import Home from '../../pages/Home';
import Search from '../../pages/Search';
import Collection from '../../pages/Collection';
import NotFound from '../../pages/NotFound';
import Section from '../../pages/Section';
import SectionTrending from '../../pages/SectionTrending';
import SectionLatest from '../../pages/SectionLatest';
import SectionDiscover from '../../pages/SectionDiscover';
import SectionSelected from '../../pages/SectionSelected';
import Tech from '../../pages/Tech';
import PopularAlbumsFromTech from '../../pages/PopularAlbumsFromTech';
import LatestAlbumsFromTech from '../../pages/LatestAlbumsFromTech';
import Album from '../../pages/Album';
// import Track from '../../pages/Track';
import Artist from '../../pages/Artist';

const App = () => {
    return (
        <Router>
            <Helmet titleTemplate='Spotify %s'/>
            <Routes>
                <Route path="/" element={<LayoutWithSidebar />}>
                    <Route index element={<Home />} />
                    <Route path="search" element={<Search />} />
                    <Route path="collection" element={<Collection />} />
                    <Route path="section" element={<Section />}>
                        <Route path="trending" element={<SectionTrending />} />
                        <Route path="latest" element={<SectionLatest />} />
                        <Route path="discover" element={<SectionDiscover />} />
                        <Route path="selected" element={<SectionSelected />} />
                    </Route>
                    <Route path="tech/:techId">
                        <Route index element={<Tech />} />
                        <Route path="albums" element={<Section />}>
                            <Route path="popular" element={<PopularAlbumsFromTech />} />
                            <Route path="new" element={<LatestAlbumsFromTech />} />
                        </Route>
                    </Route>
                    <Route path="album">
                        <Route path=":albumId" element={<Album />} />
                    </Route>
                    {/* <Route path="track">
                        <Route path=":trackId" element={<Track />} />
                    </Route> */}
                    <Route path="artist">
                        <Route path=":artistId" element={<Artist />} />
                    </Route>
                </Route>
                <Route path="*" element={<NotFound />} />
            </Routes>
        </Router>
    );
};

export default App;

// https://reactrouter.com/docs/en/v6/getting-started/tutorial
// https://stackoverflow.com/questions/34607841/react-router-nav-bar-example
// https://reactrouter.com/docs/en/v6/getting-started/concepts#outlet
// https://www.digitalocean.com/community/tutorials/how-to-handle-routing-in-react-apps-with-react-router
// https://github.com/paralect/react-starter/tree/master/src/pages
