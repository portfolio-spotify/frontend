import { useRef, useState } from 'react';

import HeaderSection from './../HeaderSection';
import CardTechno from '../Cards/CardTechno';

import styles from './ListPopularTechnos.module.css';
import { ReactComponent as ChevronLeftIcon } from '../../assets/icons/chevron-left.svg';
import { ReactComponent as ChevronRightIcon } from '../../assets/icons/chevron-right.svg';

const ListPopularTechnos = (props) => {
    const { title, technologies } = props;

    const [isScrolled, setIsScrolled] = useState(false);
    const sliderRef = useRef();

    const listTechnologies = technologies.map((technology) => {
        return (
            <CardTechno
                cardSize='big'
                key={technology.id}
                resource={technology.url}
                color={technology.color}
                name={technology.name}
                img={technology.imageURL}
            />
        );
    });

    const onClickLeft = () => {
        const width = sliderRef.current.offsetWidth;
        sliderRef.current.scrollLeft = -width / 4;
        return setIsScrolled(false);
    };

    const onClickRight = () => {
        const width = sliderRef.current.offsetWidth;
        sliderRef.current.scrollLeft = width / 4;
        return setIsScrolled(true);
    };

    const btnLeftStyle = isScrolled ? 
        `${styles.scrollBtn} ${styles.leftBtn} ${styles.active}` : 
        `${styles.scrollBtn} ${styles.leftBtn}`;
    const btnRightStyle = isScrolled ? 
        `${styles.scrollBtn} ${styles.rightBtn}` : 
        `${styles.scrollBtn} ${styles.rightBtn} ${styles.active}`;

    return (
        <section>
            <HeaderSection type="text" title={title} />
            <div className={styles.popular}>
                <div className={styles.popularListContainer} ref={sliderRef}>
                    <button className={btnLeftStyle} onClick={onClickLeft}>
                        <ChevronLeftIcon />
                    </button>
                    <ul className={styles.popularList}>
                        {listTechnologies}
                    </ul>
                    <button className={btnRightStyle} onClick={onClickRight}>
                        <ChevronRightIcon />
                    </button>
                </div>
            </div>
        </section>
    );
};

export default ListPopularTechnos;