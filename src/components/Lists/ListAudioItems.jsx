import HeaderSection from './../HeaderSection';
import AudioItem from './AudioItem';

import styles from './ListAudioItems.module.css';

const ListAudioItems = (props) => {
    const { headerType, title, to, items } = props;

    return (
        <section className={styles.homeSection}>
            {
                headerType === 'link' ?
                <HeaderSection type="link" to={to} title={title} /> :
                <HeaderSection type="text" title={title} />
            }

            <ul className={styles.listItems}>
                { items.map((item) => <AudioItem key={item.id} album={item} />) }
            </ul>
        </section>
    );
};

export default ListAudioItems;
