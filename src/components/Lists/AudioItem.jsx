import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

import useAudioPlayerStore from '../../stores/useAudioPlayerStore';

import styles from './AudioItem.module.css';

import ButtonPrimary from '../Buttons/ButtonPrimary';

const AudioItem = (props) => {
    const { album } = props;

    const isPlaying = useAudioPlayerStore((state) => state.isPlaying);
    const currentPlayingAlbum = useAudioPlayerStore((state) => state.album);
    const setIsPlaying = useAudioPlayerStore((state) => state.setIsPlaying);
    const setTracks = useAudioPlayerStore((state) => state.setTracks);
    const setAlbum = useAudioPlayerStore((state) => state.setAlbum);
    const setAuthor = useAudioPlayerStore((state) => state.setAuthor);
    const setTrackIndex = useAudioPlayerStore((state) => state.setTrackIndex);
    const [isAlbumPlaying, setIsAlbumPlaying] = useState(false);

    useEffect(() => {
        if (isPlaying) {
            const isCurrentAlbumPlaying = album.id === currentPlayingAlbum.id;
    
            if (isCurrentAlbumPlaying) return setIsAlbumPlaying(true);
        }

        setIsAlbumPlaying(false);
    }, [isPlaying, album.id, currentPlayingAlbum.id]);
    
    const togglePlayPause = async () => {
        const isCurrentAlbumPlaying = album.id === currentPlayingAlbum.id;

        if (isPlaying && isCurrentAlbumPlaying) return setIsPlaying(false);

        if (!isPlaying && isCurrentAlbumPlaying) return setIsPlaying(true);

        const fetchAuthor = fetch(`/api/users/${album.author_id}`);
        const fetchTracks = fetch(`/api/projects/${album.url}/tracks`);

        const [author, tracks] = await Promise.all([fetchAuthor, fetchTracks]);

        if (!author.ok || !tracks.ok) console.log('ERROR AUTHOR OR TRACKS');

        const [authorJson, tracksJson] = await Promise.all([author.json(), tracks.json()]);

        setAlbum(album);
        setTracks(tracksJson.data);
        setAuthor(authorJson.data);
        setIsPlaying(true);
        setTrackIndex(0);
    };

    const itemStyle = isAlbumPlaying ? `${styles.item} ${styles.playing}` : `${styles.item}`;

    return (
        <li className={itemStyle}>
            <Link to={`/album/${album.url}`} />
            <div className={styles.imgContainer}>
                <img src={album.imageURL} alt="cover" />
                    <ButtonPrimary 
                        size="medium" 
                        onClick={togglePlayPause} 
                        isPlaying={isAlbumPlaying} 
                    />
            </div>
            <span className={styles.title}>{album.title}</span>
            <p className={styles.description}>
                {album.description}
            </p>
        </li>
    );
};

export default AudioItem;
