import HeaderSection from '../HeaderSection';
import CardTechno from '../Cards/CardTechno';

import styles from './ListAllTechnos.module.css';

const ListAllTechnos = (props) => {
    const { title, technologies } = props;

    const listTechnologies = technologies.map((technology) => {
        return (
            <CardTechno
                cardSize='small'
                key={technology.id}
                resource={technology.url} 
                color={technology.color} 
                name={technology.name} 
                img={technology.imageURL} 
            />
        );
    });

    return (
        <section className={styles.all}>
            <HeaderSection type="text" title={title} />
            <ul className={styles.allList}>
                {listTechnologies}
            </ul>
        </section>
    );
};

export default ListAllTechnos;