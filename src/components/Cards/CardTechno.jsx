import { NavLink } from 'react-router-dom';

import styles from './CardTechno.module.css';

const CardTechno = (props) => {
    const { cardSize, resource, color, name, img } = props;

    const cardSizeClass = cardSize === 'big' ? styles.cardBig : styles.cardSmall;

    return (
        <li className={`${styles.techContainer} ${cardSizeClass}`} > 
            <NavLink to={`/tech/${resource}`} className={styles.tech} style={{backgroundColor: `${color}`}}>
                <span className={styles.title}>{name}</span>
                <div className={styles.techLogoContainer}>
                    <img className={styles.techLogo} src={img} alt="technology logo" />
                </div>
            </NavLink>
        </li>
    );
};

export default CardTechno;
