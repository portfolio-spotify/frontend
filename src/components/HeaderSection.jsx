import { NavLink } from 'react-router-dom';

import styles from './HeaderSection.module.css';

const HeaderSection = (props) => {
    const { to, title, type } = props;

    let header;

    if (type === 'text') {
        header = (
            <header className={styles.header}>
                <h5>{title}</h5>
            </header>
        )
    }

    if (type === 'link') {
        header = (
            <header className={styles.header}>
                <NavLink to={to}>
                    <h5>{title}</h5>
                </NavLink>
                
                <NavLink to={to} className={styles.seeAll}>
                    VOIR TOUT
                </NavLink>
            </header>
        )
    }

    return (
        <>
            {header}
        </>
    );
};

export default HeaderSection;