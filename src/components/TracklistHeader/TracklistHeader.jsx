import styles from './TracklistHeader.module.css';

import { ReactComponent as LogoClock } from './../../assets/icons/clock.svg';

const TracklistHeader = () => {
    return (
        <ul className={styles.tracksHeader}>
            <li className={styles.tracksNumbers}>#</li>
            <li className={styles.tracksTitles}>titre</li>
            <li className={styles.tracksReleases}>ajouté le</li>
            <li className={styles.tracksPlays}>écoutes</li>
            <li className={styles.tracksDurations}>
                <LogoClock />
            </li>
        </ul>
    );
};

export default TracklistHeader;