import styles from './Overlay.module.css';

const Overlay = ({ color }) => {
    const inlineStyle = {
        background: `linear-gradient(to bottom, rgba(0, 0, 0, 0.6), rgba(18, 18, 18, 1)),
                     linear-gradient(${color}, ${color})`
    }

    return (
        <div className={styles.overlay} style={inlineStyle} ></div>
    );
};

export default Overlay;