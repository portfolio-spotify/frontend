import styles from './ContainerProductDetails.module.css';

const ContainerProductDetails = (props) => {
    return (
        <section className={styles.container}>
            {props.children}
        </section>
    );
};

export default ContainerProductDetails;