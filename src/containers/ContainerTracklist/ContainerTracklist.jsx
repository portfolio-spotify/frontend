import styles from './ContainerTracklist.module.css';

const ContainerTracklist = (props) => {
    return (
        <ul className={styles.container}>
            {props.children}
        </ul>
    );
};

export default ContainerTracklist;