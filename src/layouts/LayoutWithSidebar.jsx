import { Outlet } from 'react-router-dom';

import Sidebar from '../components/Sidebar/Sidebar';
import AudioPlayer from '../components/AudioPlayer/AudioPlayer';

import styles from './LayoutWithSidebar.module.css';

const LayoutWithSidebar = () => {
    return (
        <section className={styles.base}>
            <Sidebar />
            <section className={styles.main}>
                <Outlet />
            </section>
            <AudioPlayer />
        </section>
    );
};

export default LayoutWithSidebar;