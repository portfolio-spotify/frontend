import { useParams } from 'react-router-dom';

import useTechnology from '../hooks/useTechnology';
import useTechnologyLatestAlbums from '../hooks/useTechnologyLatestAlbums';
import ListAudioItems from '../components/Lists/ListAudioItems';

const LatestAlbumsFromTech = () => {
    const { techId } = useParams();

    const { idLoading: isTechLoading, error: errorTech, data: dataTech } = useTechnology(techId);
    const { isLoading, error, data } = useTechnologyLatestAlbums(techId);

    if (isLoading || isTechLoading) return 'Chargement...';

    if (error || errorTech) return `Une erreur est survenue : ${error.message}`;

    const { data: latestAlbums } = data;
    const { data: technology } = dataTech;

    return (
        <ListAudioItems 
            headerType='text' 
            title={`Nouveautés avec ${technology.name}`} 
            items={latestAlbums} 
        />
    );
};

export default LatestAlbumsFromTech;
