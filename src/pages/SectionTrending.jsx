import useSection from '../hooks/useSection';
import ListAudioItems from '../components/Lists/ListAudioItems';

const SectionTrending = () => {
    const { isLoading, error, data } = useSection('trending');

    if (isLoading) return 'Chargement...';

    if (error) return `Une erreur est survenue : ${error.message}`;

    const { data: trending } = data;

    return (
        <ListAudioItems headerType='text' title={`À ne pas manquer aujourd'hui !`} items={trending} />
    );
};

export default SectionTrending;
