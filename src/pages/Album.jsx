import { useParams, Navigate } from 'react-router-dom';
import { Helmet } from 'react-helmet-async';

import useAlbum from '../hooks/useAlbum';

import ProductBanner from '../features/Product/Banner/ProductBanner';
import ContainerProductDetails from '../containers/ContainerProductDetails/ContainerProductDetails';
import Overlay from '../components/Overlay/Overlay';
import ProductActions from '../features/Product/Actions/ProductActions';
import TracklistHeader from '../components/TracklistHeader/TracklistHeader';
import ContainerTracklist from '../containers/ContainerTracklist/ContainerTracklist';

import TrackAlbum from '../components/Track/TrackAlbum';

const Album = () => {
    const { albumId } = useParams();

    const { isLoading, error, data } = useAlbum(albumId);

    if (error) {
        if (error.error.statusCode === 404) return <Navigate replace to="/not-found" />;

        return `Une erreur est survenue : ${error.error.messages[0].text}`;
    }

    if (isLoading) return 'Chargement...';

    const album = data.project.data;
    const user = data.user.data;
    const tracks = data.tracks.data;

    return (
        <>
            <Helmet title={`| ${album.title}`} />
            <ProductBanner album={album} tracks={tracks} user={user} type="album" />
            <ContainerProductDetails>
                <Overlay color={album.color} />
                <ProductActions album={album} tracks={tracks} user={user} />
                <TracklistHeader />

                <ContainerTracklist>
                    { tracks.map((track, index) => (
                        <TrackAlbum 
                            key={track.id}
                            currentTrackIndex={index}
                            albumTracks={tracks}
                            track={track}
                            album={album} 
                            user={user} 
                        />
                    )) }
                </ContainerTracklist>
                    
            </ContainerProductDetails>
        </>
    );
};

export default Album;
