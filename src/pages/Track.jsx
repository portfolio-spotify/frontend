import { useEffect } from 'react';
import { NavLink } from 'react-router-dom';

import ProductBanner from '../features/Product/Banner/ProductBanner';
import ContainerProductDetails from '../containers/ContainerProductDetails/ContainerProductDetails';
import Overlay from '../components/Overlay/Overlay';
import ProductActions from '../features/Product/Actions/ProductActions';
import ContainerTracklist from '../containers/ContainerTracklist/ContainerTracklist';
import { default as TrackContainer } from '../components/Track/Track';
import TrackNumber from '../components/Track/TrackNumber/TrackNumber';
import TrackDetails from '../components/Track/TrackDetails/TrackDetails';
import TrackListened from '../components/Track/TrackListened/TrackListened';
import TrackDuration from '../components/Track/TrackDuration/TrackDuration';
import ListAudioItems from '../components/Lists/ListAudioItems';

import styles from './styles.module.css';

import artistImg from '../assets/images/artist.jpg';
import productImg from '../assets/images/album.jpg';

import image1 from './../assets/images/image1.png';
import image2 from './../assets/images/image2.png';
import image3 from './../assets/images/image3.png';
import image4 from './../assets/images/image4.png';
import image5 from './../assets/images/image5.png';
import image6 from './../assets/images/image6.png';

const product = {
    img: productImg,
    title: 'Intro',
    artistImg: artistImg,
    artistLink: '7CajNmpbOovFoOoasH2HaY',
    artistName: 'Calvin Harris',
    releaseYear: 2022,
    lengthTotal: '0:39',
    color: '#3088A8',
};

const popularMixed = {
    title: `Sorties populaires par Calvin Harris`,
    resource: 'kkj23SIdd4Xi',
    items: [
        {
            id: '1',
            resource: 'hksM1jwSrP',
            img: image1,
            title: 'One Kiss (with Dua Lipa)',
            description: '2018 • Single'
        },
        {
            id: '2',
            resource: 'rW0RN8P7f5',
            img: image2,
            title: 'Motion',
            description: '2014 • Album'
        },
        {
            id: '3',
            resource: 'wcVSd5qe6l',
            img: image3,
            title: 'Kick back to the best new and recent chill hits.',
            description: '2014 • Album'
        },
        {
            id: '4',
            resource: '2JgsvEBEeD',
            img: image4,
            title: '18 Months',
            description: '2019 • Album'
        },
        {
            id: '5',
            resource: 'ez6Ltsihbp',
            img: image5,
            title: 'Potion (with Dua Lipa & Young Thug)',
            description: '2014 • Single'
        },
        {
            id: '6',
            resource: 'j2EbsSzqhU',
            img: image6,
            title: 'Calm Before the Storm',
            description: `2014 • Album`
        },
    ]
};

const popularSingles = {
    title: `Singles populaires par Calvin Harris`,
    resource: 'kkj23SIdd4Xi',
    items: [
        {
            id: '1',
            resource: 'hksM1jwSrP',
            img: image1,
            title: 'One Kiss (with Dua Lipa)',
            description: '2018 • Single'
        },
        {
            id: '2',
            resource: 'rW0RN8P7f5',
            img: image2,
            title: 'Motion',
            description: '2014 • Single'
        },
        {
            id: '3',
            resource: 'wcVSd5qe6l',
            img: image3,
            title: 'Kick back to the best new and recent chill hits.',
            description: '2014 • Single'
        },
        {
            id: '4',
            resource: '2JgsvEBEeD',
            img: image4,
            title: '18 Months',
            description: '2019 • Single'
        },
        {
            id: '5',
            resource: 'ez6Ltsihbp',
            img: image5,
            title: 'Potion (with Dua Lipa & Young Thug)',
            description: '2014 • Single'
        },
        {
            id: '6',
            resource: 'j2EbsSzqhU',
            img: image6,
            title: 'Calm Before the Storm',
            description: `2014 • Single`
        },
    ]
};

const popularAlbum = {
    title: `Albums populaires par Calvin Harris`,
    resource: 'kkj23SIdd4Xi',
    items: [
        {
            id: '1',
            resource: 'hksM1jwSrP',
            img: image1,
            title: 'One Kiss (with Dua Lipa)',
            description: '2018 • Album'
        },
        {
            id: '2',
            resource: 'rW0RN8P7f5',
            img: image2,
            title: 'Motion',
            description: '2014 • Album'
        },
        {
            id: '3',
            resource: 'wcVSd5qe6l',
            img: image3,
            title: 'Kick back to the best new and recent chill hits.',
            description: '2014 • Album'
        },
        {
            id: '4',
            resource: '2JgsvEBEeD',
            img: image4,
            title: '18 Months',
            description: '2019 • Album'
        },
        {
            id: '5',
            resource: 'ez6Ltsihbp',
            img: image5,
            title: 'Potion (with Dua Lipa & Young Thug)',
            description: '2014 • Album'
        },
        {
            id: '6',
            resource: 'j2EbsSzqhU',
            img: image6,
            title: 'Calm Before the Storm',
            description: `2014 • Album`
        },
    ]
};

const Track = () => {
    useEffect(() => {
        document.title = 'Spotify | Intro by Calvin Harris';
    }, []);

    return (
        <>
            <ProductBanner album={product} type="titre" />
            <ContainerProductDetails>
                <Overlay color={product.color} />
                <ProductActions />

                {/* Artist card */}
                <NavLink to={`/artist/${product.artistLink}`} className={styles.cardArtist}>
                    <img src={product.artistImg} alt="artist" className={styles.artistPhoto}/>
                    <div className={styles.artistInfos}>
                        <span>artiste</span>
                        <span>{product.artistName}</span>
                    </div>
                </NavLink>

                {/* Popular titles  */}
                <section className={styles.popularSingles}>
                    <span>Titres populaires par</span>
                    <h5 className={styles.popularArtist}>Calvin Harris</h5>
                    <ContainerTracklist>
                        <TrackContainer>
                            <TrackNumber number="1" />
                            <img src={product.img} alt="cover title" className={styles.singleCover} />
                            <TrackDetails title="New Money" />
                            <TrackListened listenedTotal="999 002" />
                            <TrackDuration duration="2:48" />
                        </TrackContainer>

                        <TrackContainer>
                            <TrackNumber number="2" />
                            <img src={product.img} alt="cover title" className={styles.singleCover} />
                            <TrackDetails title="One Kiss (with Dua Lipa)" />
                            <TrackListened listenedTotal="1 000 201" />
                            <TrackDuration duration="3:51" />
                        </TrackContainer>
                        
                        <TrackContainer>
                            <TrackNumber number="3" />
                            <img src={product.img} alt="cover title" className={styles.singleCover} />
                            <TrackDetails title="Potion (with Dua Lipa & Young Thug)" />
                            <TrackListened listenedTotal="890 243" />
                            <TrackDuration duration="3:35" />
                        </TrackContainer>
                    </ContainerTracklist>
                </section>

                <ListAudioItems headerType="text" title={popularMixed.title} to={popularMixed.resource} items={popularMixed.items} />
                <ListAudioItems headerType="text" title={popularAlbum.title} to={popularAlbum.resource} items={popularAlbum.items} />
                <ListAudioItems headerType="text" title={popularSingles.title} to={popularSingles.resource} items={popularSingles.items} />

                <section className={styles.fromTheAlbum}>
                    <NavLink to={`/album/sdsAEKXMSKCx`} className={styles.fromAlbumHeader}>
                        <img src={product.img} alt="cover album" />
                        <div className={styles.fromAlbumDetails}>
                            <span className="description">Issu de l'album</span>
                            <span className="name">Funk Wav Bounces Vol. 2</span>
                        </div>
                    </NavLink>
                    <ContainerTracklist>
                        <TrackContainer>
                            <TrackNumber number="1" />
                            <TrackDetails title="New Money" artist="Calvin Harris"/>
                            <TrackDuration duration="2:48" />
                        </TrackContainer>

                        <TrackContainer>
                            <TrackNumber number="2" />
                            <TrackDetails title="One Kiss (with Dua Lipa)" artist="Calvin Harris, 21 Savage"/>
                            <TrackDuration duration="3:51" />
                        </TrackContainer>
                        
                        <TrackContainer>
                            <TrackNumber number="3" />
                            <TrackDetails title="Potion (with Dua Lipa & Young Thug)" artist="Calvin Harris, Dua Lipa, Young Thug"/>
                            <TrackDuration duration="3:35" />
                        </TrackContainer>
                    </ContainerTracklist>
                </section>

            </ContainerProductDetails>
        </>
    );
};

export default Track;