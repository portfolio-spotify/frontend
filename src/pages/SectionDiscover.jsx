import useSection from '../hooks/useSection';
import ListAudioItems from '../components/Lists/ListAudioItems';

const SectionDiscover = () => {
    const { isLoading, error, data } = useSection('discover');

    if (isLoading) return 'Chargement...';

    if (error) return `Une erreur est survenue : ${error.message}`;

    const { data: discover } = data;

    return (
        <ListAudioItems headerType='text' title='À découvrir' items={discover} />
    );
};

export default SectionDiscover;
