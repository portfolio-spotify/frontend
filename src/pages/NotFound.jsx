import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet-async';

import { ReactComponent as LogoSpotify } from './../assets/icons/spotify-logo.svg';

import styles from './styles.module.css';

const NotFound = () => {
    return (
        <>
            <Helmet defaultTitle='Page introuvable' />
            <section className={styles.notFound}>
                <LogoSpotify title="Spotify" />
                <h3>Page introuvable</h3>
                <p>Nous ne trouvons pas la page que vous recherchez.</p>
                <Link to="/">Accueil</Link>
            </section>
        </>
    );
};

export default NotFound;