import { useParams, Navigate } from 'react-router-dom';
import { Helmet } from 'react-helmet-async';

import usePageTechnology from '../hooks/usePageTechnology';
import ListAudioItems from '../components/Lists/ListAudioItems';

import styles from './styles.module.css';

const Tech = () => {
    const { techId } = useParams();

    const { isLoading, error, data } = usePageTechnology(techId, 6);

    if (error) {
        if (error.error.statusCode === 404) return <Navigate replace to="/not-found" />;

        return `Une erreur est survenue : ${error.error.messages[0].text}`;
    }

    if (isLoading) return 'Chargement...';

    const technology = data.technology.data;
    const mostListenedAlbums = data.mostListenedAlbums.data;
    const latestAlbums = data.latestAlbums.data;

    return (
        <>
            <Helmet title={`| ${technology.name}`} />
            <section className={styles.layout}>
                <h1 className={styles.titleTech}>{technology.name}</h1>
                <ListAudioItems 
                    headerType='link' 
                    title="Populaires"
                    to={`/tech/${techId}/albums/popular`}
                    items={mostListenedAlbums} 
                />
                <ListAudioItems 
                    headerType='link' 
                    title="Nouveautés"
                    to={`/tech/${techId}/albums/new`} 
                    items={latestAlbums} 
                />
            </section>
        </>
    );
};

export default Tech;