import useSection from '../hooks/useSection';
import ListAudioItems from '../components/Lists/ListAudioItems';

const SectionLatest = () => {
    const { isLoading, error, data } = useSection('latest');

    if (isLoading) return 'Chargement...';

    if (error) return `Une erreur est survenue : ${error.message}`;

    const { data: latest } = data;

    return (
        <ListAudioItems headerType='text' title='Dernières sorties' items={latest} />
    );
};

export default SectionLatest;
