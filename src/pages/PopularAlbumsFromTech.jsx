import { useParams } from 'react-router-dom';

import useTechnologyMostListenedAlbums from '../hooks/useTechnologyMostListenedAlbums';
import useTechnology from '../hooks/useTechnology';
import ListAudioItems from '../components/Lists/ListAudioItems';

const PopularAlbumsFromTech = () => {
    const { techId } = useParams();

    const { idLoading: isTechLoading, error: errorTech, data: dataTech } = useTechnology(techId);
    const { isLoading, error, data } = useTechnologyMostListenedAlbums(techId);

    if (isLoading || isTechLoading) return 'Chargement...';

    if (error || errorTech) return `Une erreur est survenue : ${error.message}`;

    const { data: mostListenedAlbums } = data;
    const { data: technology } = dataTech;

    return (
        <ListAudioItems 
            headerType='text' 
            title={`Projets populaires avec ${technology.name}`} 
            items={mostListenedAlbums} 
        />
    );
};

export default PopularAlbumsFromTech;
