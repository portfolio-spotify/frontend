import { Helmet } from 'react-helmet-async';

import useSections from '../hooks/useSections';
import ListAudioItems from './../components/Lists/ListAudioItems';

import styles from './styles.module.css';

const Home = () => {
    const { isLoading, error, data } = useSections();

    if (isLoading) return 'Chargement...';

    if (error) return `Une erreur est survenue : ${error.message}`;

    const { trending, latest, selected, discover } = data.data;

    return (
        <>
            <Helmet title=""/>
            <section className={styles.layout}>
                <ListAudioItems headerType='link' title={`À ne pas manquer aujourd'hui !`} to='/section/trending' items={trending} />
                <ListAudioItems headerType='link' title='Dernières sorties' to='/section/latest' items={latest} />
                <ListAudioItems headerType='link' title='À découvrir' to='/section/discover' items={discover} />
                <ListAudioItems headerType='link' title='Notre sélection' to='/section/selected' items={selected} />
            </section>
        </>
    );
};

export default Home;
