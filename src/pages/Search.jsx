import { Helmet } from 'react-helmet-async';
import orderBy from 'lodash/orderBy';

import useTechnologies from '../hooks/useTechnologies';
import ListAllTechnos from './../components/Lists/ListAllTechnos';
import ListPopularTechnos from './../components/Lists/ListPopularTechnos';

import styles from './styles.module.css';

const Search = () => {
    const { isLoading, error, data } = useTechnologies();

    if (isLoading) return 'Chargement...';

    if (error) return `Une erreur est survenue : ${error.message}`;

    const technologies = data.data;

    const sortedTechnologies = orderBy(technologies, ['total_usage'], ['desc']);

    const popularTechnologies = sortedTechnologies.slice(0, 4);

    return (
        <>
            <Helmet title='| Rechercher' />
            <section className={styles.layout}>
                <ListPopularTechnos title='Les plus utilisées' technologies={popularTechnologies} />
                <ListAllTechnos title='Parcourir tout' technologies={technologies} />
            </section>
        </>
    );
};

export default Search;