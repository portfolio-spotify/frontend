import { useParams, Navigate } from 'react-router-dom';
import { Helmet } from 'react-helmet-async';
import capitalize from 'lodash/capitalize';

import usePageProfile from '../hooks/usePageProfile';

import UserBanner from '../features/User/Banner/UserBanner';
import ContainerProductDetails from '../containers/ContainerProductDetails/ContainerProductDetails';
import Overlay from '../components/Overlay/Overlay';
import UserActions from '../features/User/Actions/UserActions';
import ContainerTracklist from '../containers/ContainerTracklist/ContainerTracklist';
import ListAudioItems from '../components/Lists/ListAudioItems';
import TrackProfile from '../components/Track/TrackProfile';

import styles from './styles.module.css';

const Artist = () => {
    const { artistId } = useParams();

    const { isLoading, error, data } = usePageProfile(artistId, 6);

    if (error) {
        if (error.error.statusCode === 404) return <Navigate replace to="/not-found" />;

        return `Une erreur est survenue : ${error.error.messages[0].text}`;
    }

    if (isLoading) return 'Chargement...';

    const user = data.user.data;
    const mostListenedAlbums = data.mostListenedAlbums.data;
    const latestAlbums = data.latestAlbums.data;
    const totalAlbums = data.totalAlbums.data;
    const mostListenedTracksTotal = data.mostListenedTracks.data;
    const mostListenedTracksSample = mostListenedTracksTotal.slice(0, 3);

    console.log('TRACKS', mostListenedTracksTotal)

    const authorName = `${capitalize(user.first_name)} ${capitalize(user.last_name)}`;
    // TODO : ajouter PLAY/PAUSE dans UserActions
    // TODO : modifier ListAudioItems pour inclure play d'une track

    return (
        <>
            <Helmet title={`| ${authorName}`} />
            <UserBanner 
                coverImage={user.image_coverURL} 
                name={authorName} 
                totalProjects={totalAlbums.total} 
            />
            <ContainerProductDetails>
                <Overlay color={user.color} />
                <UserActions />

                {/* Popular titles && artist selection */}
                <section className={styles.popular}>
                    <h5>Populaires</h5>
                    <ContainerTracklist>
                        { mostListenedTracksSample.map((track, index) => (
                            <TrackProfile 
                                key={track.id}
                                track={track}
                                trackNumber={index + 1}
                            />
                        ))}
                    </ContainerTracklist>
                </section>

                <ListAudioItems 
                    headerType="text" 
                    title="Dernières sorties"
                    items={latestAlbums}
                />
                <ListAudioItems 
                    headerType="text" 
                    title="Projets populaires"
                    items={mostListenedAlbums}
                />
                <ListAudioItems 
                    headerType="text" 
                    title="Titres populaires" 
                    items={mostListenedTracksTotal}
                />
                    
                <section className={styles.about}>
                    <h5>Plus d'infos</h5>
                    <div className="content">
                        <img src={user.image_descriptionURL} alt="artist" />
                        <article className="text">
                            <span className="title">{totalAlbums.total} projets ajoutés</span>
                            <p className="description">{user.description}</p>
                        </article>
                    </div>
                </section>
            </ContainerProductDetails>
        </>
    );
};

export default Artist;
