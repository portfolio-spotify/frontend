import useSection from '../hooks/useSection';
import ListAudioItems from '../components/Lists/ListAudioItems';

const SectionSelected = () => {
    const { isLoading, error, data } = useSection('selected');

    if (isLoading) return 'Chargement...';

    if (error) return `Une erreur est survenue : ${error.message}`;

    const { data: selected } = data;

    return (
        <ListAudioItems headerType='text' title='Notre sélection' items={selected} />
    );
};

export default SectionSelected;
