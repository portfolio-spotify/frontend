import { Outlet } from 'react-router-dom';

import styles from './styles.module.css';

const Section = () => {
    return (
        <section className={styles.layout}>
            <Outlet />
        </section>
    );
};

export default Section;