import create from 'zustand';

import { getAudioRepeatMode } from '../utils/audio';

const getPrevStateDefault0 = (key) => {
    const value = JSON.parse(localStorage.getItem(key));

    if (value === null) return 1;

    return value;
};

const useAudioPlayerStore = create((set) => ({
    album: JSON.parse(localStorage.getItem('prevAlbum')) || null,
    author: JSON.parse(localStorage.getItem('prevAuthor')) || null,
    tracks: JSON.parse(localStorage.getItem('prevTracks')) || [],
    isPlaying: false,
    isRandom: JSON.parse(localStorage.getItem('wasRandom')) || false,
    repeating : JSON.parse(localStorage.getItem('wasRepeating')) || 'none',
    trackIndex: JSON.parse(localStorage.getItem('prevTrackIndex')) || 0,
    trackProgress: JSON.parse(localStorage.getItem('prevTrackProgress')) || 0,
    soundProgress: getPrevStateDefault0('prevSoundProgress'),
    isMuted: JSON.parse(localStorage.getItem('wasMuted')) || false,
    trackMutedFrom: getPrevStateDefault0('prevTrackMutedFrom'),
    likedAudios: JSON.parse(localStorage.getItem('likedAudios')) || [],
    likedAlbums: JSON.parse(localStorage.getItem('likedAlbums')) || [],
    setAlbum: (album) => set(() => ({ album })),
    setAuthor: (author) => set(() => ({ author })),
    setTracks: (tracks) => set(() => ({ tracks })),
    setIsPlaying: (playing) => set(() => ({ isPlaying: playing })),
    setIsRandom: (random) => set(() => ({ isRandom: random })),
    setRepeating: (mode) => set((state) => {
        if (mode) return { repeating: mode };

        const nextMode = getAudioRepeatMode(state.repeating);

        return { repeating: nextMode };
    }),
    setTrackIndex: (index) => set(() => ({ trackIndex: index })),
    setTrackProgress: (progress) => set(() => ({ trackProgress: progress })),
    setSoundProgress: (progress) => set(() => ({ soundProgress: progress })),
    setTrackMutedFrom: (from) => set(() => ({ trackMutedFrom: from })),
    setIsMuted: (isMuted) => set(() => ({ isMuted })),
    setLikedAudios: (audiosIds) => set(() => {
        localStorage.setItem('likedAudios', JSON.stringify([...audiosIds]));

        return {
            likedAudios: audiosIds
        };
    }),
    setLikedAlbums: (albumsIds) => set(() => {
        localStorage.setItem('likedAlbums', JSON.stringify([...albumsIds]));

        return {
            likedAlbums: albumsIds
        };
    })
}));

export default useAudioPlayerStore;
