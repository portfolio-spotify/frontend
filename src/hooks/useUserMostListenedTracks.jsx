import { useQuery } from '@tanstack/react-query';

const getUserMostListenedTracks = async (id, limit) => {
    const limitParam = limit ? `?limit=${limit}` : '';

    const response = await fetch(`/api/users/${id}/tracks/popular${limitParam}`);

    const responseJson = await response.json();

    if (!response.ok) return Promise.reject(responseJson);

    return responseJson;
};

const useUserMostListenedTracks = (id, limit) => useQuery(
    ['useUserMostListenedTracks', id], 
    () => getUserMostListenedTracks(id, limit)
);

export default useUserMostListenedTracks;
