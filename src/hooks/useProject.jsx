import { useQuery } from '@tanstack/react-query';

const getProject = async (id) => {
    const response = await fetch(`/api/projects/${id}`);

    const responseJson = await response.json();

    if (!response.ok) return Promise.reject(responseJson);

    return responseJson;
};

const useProject = (id) => useQuery(['album', id], () => getProject(id));

export default useProject;
