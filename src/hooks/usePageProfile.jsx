import useUser from './useUser';
import useUserMostListenedAlbums from './useUserMostListenedAlbums';
import useUserLatestAlbums from './useUserLatestAlbums';
import useUserTotalAlbums from './useUserTotalAlbums';
import useUserMostListenedTracks from './useUserMostListenedTracks';

const usePageProfile = (userId, limit) => {
    const {
        isLoading: isLoadingUser,
        error: errorUser,
        data: user
    } = useUser(userId);

    const {
        isLoading: isLoadingMostListenedAlbums,
        error: errorMostListenedAlbums,
        data: mostListenedAlbums
    } = useUserMostListenedAlbums(userId, limit);

    const {
        isLoading: isLoadingLatest,
        error: errorLatest,
        data: latest
    } = useUserLatestAlbums(userId, limit);

    const {
        isLoading: isLoadingTotal,
        error: errorTotal,
        data: total
    } = useUserTotalAlbums(userId);

    const {
        isLoading: isLoadingMostListenedTracks,
        error: errorMostListenedTracks,
        data: mostListenedTracks
    } = useUserMostListenedTracks(userId, limit)

    const isLoading = 
        isLoadingUser || 
        isLoadingMostListenedAlbums || 
        isLoadingLatest || 
        isLoadingTotal ||
        isLoadingMostListenedTracks
    ;

    const error = 
        errorUser || 
        errorMostListenedAlbums || 
        errorLatest || 
        errorTotal || 
        errorMostListenedTracks
    ;

    return {
        isLoading,
        error,
        data: {
            user,
            mostListenedAlbums,
            latestAlbums: latest,
            totalAlbums: total,
            mostListenedTracks
        }
    };
};

export default usePageProfile;
