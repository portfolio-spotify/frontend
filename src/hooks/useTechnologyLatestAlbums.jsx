import { useQuery } from '@tanstack/react-query';

const getTechnologyLatestAlbums = async (id, limit) => {
    const limitParam = limit ? `?limit=${limit}` : '';

    const response = await fetch(`/api/technologies/${id}/projects/new${limitParam}`);

    const responseJson = await response.json();

    if (!response.ok) return Promise.reject(responseJson);

    return responseJson;
};

const useTechnologyLatestAlbums = (id, limit) => useQuery(
    ['technologyLatestAlbums', id], 
    () => getTechnologyLatestAlbums(id, limit)
);

export default useTechnologyLatestAlbums;
