import { useQuery } from '@tanstack/react-query';

const getSections = async () => {
    const response = await fetch('/api/sections?limit=6');

    const responseJson = await response.json();

    if (!response.ok) return Promise.reject(responseJson);

    return responseJson;
};

const useSections = () => useQuery(['sectionsData'], getSections);

export default useSections;
