import { useQuery } from '@tanstack/react-query';

const getTechnology = async (id) => {
    const response = await fetch(`/api/technologies/${id}`);

    const responseJson = await response.json();

    if (!response.ok) return Promise.reject(responseJson);

    return responseJson;
};

const useTechnology = (id) => useQuery(['technology', id], () => getTechnology(id));

export default useTechnology;
