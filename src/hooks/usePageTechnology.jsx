import useTechnology from './useTechnology';
import useTechnologyLatestAlbums from './useTechnologyLatestAlbums';
import useTechnologyMostListenedAlbums from './useTechnologyMostListenedAlbums';

const usePageTechnology = (technologyId, limit) => {
    const { 
        isLoading: isLoadingTechnology, 
        error: errorTechnology, 
        data: technology 
    } = useTechnology(technologyId);
    
    const {
        isLoading: isLoadingMostListened,
        error: errorMostListened,
        data: mostListened
    } = useTechnologyMostListenedAlbums(technologyId, limit);
    
    const {
        isLoading: isLoadingLatest,
        error: errorLatest,
        data: latest
    } = useTechnologyLatestAlbums(technologyId, limit);

    const isLoading = isLoadingTechnology || isLoadingMostListened || isLoadingLatest;

    const error = errorTechnology || errorMostListened || errorLatest;

    return {
        isLoading,
        error,
        data: {
            technology,
            mostListenedAlbums: mostListened,
            latestAlbums: latest
        }
    };
};

export default usePageTechnology;
