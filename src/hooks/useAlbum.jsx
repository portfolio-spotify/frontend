import useProject from './useProject';
import useUser from './useUser';
import useProjectTracks from './useProjectTracks';

const useAlbum = (albumId) => {
    const { isLoading: isLoadingProject, error: errorProject, data: project } = useProject(albumId);

    const userId = project?.data?.author_id;

    const { isLoading: isLoadingUser, error: errorUser, data: user, fetchStatus: fetchStatusUser } = useUser(userId);
    const { isLoading: isLoadingTracks, error: errorTracks, data: tracks } = useProjectTracks(albumId);
    
    /* Because useUser depends on the userId from useProject */
    const isLoading = isLoadingProject || (isLoadingUser && fetchStatusUser !== 'idle') || isLoadingTracks;
    
    const error = errorProject || errorUser || errorTracks;

    return {
        isLoading,
        error,
        data: {
            project,
            user,
            tracks
        }
    };
};

export default useAlbum;
