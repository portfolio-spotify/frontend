import { useQuery } from '@tanstack/react-query';

const getSection = async (name) => {
    const response = await fetch(`/api/sections/${name}`);

    const responseJson = await response.json();

    if (!response.ok) return Promise.reject(responseJson);

    return responseJson;
};

const useSection = (name) => useQuery(['section', name], () => getSection(name));

export default useSection;