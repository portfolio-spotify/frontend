import { useQuery } from '@tanstack/react-query';

const getTechnologies = async () => {
    const response = await fetch('/api/technologies');

    const responseJson = await response.json();

    if (!response.ok) return Promise.reject(responseJson);

    return responseJson;
};

const useTechnologies = () => useQuery(['technologies'], getTechnologies);

export default useTechnologies;
