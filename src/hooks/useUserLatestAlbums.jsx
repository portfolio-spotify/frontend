import { useQuery } from '@tanstack/react-query';

const getUserLatestAlbums = async (id, limit) => {
    const limitParam = limit ? `?limit=${limit}` : '';

    const response = await fetch(`/api/users/${id}/projects/new${limitParam}`);

    const responseJson = await response.json();

    if (!response.ok) return Promise.reject(responseJson);

    return responseJson;
};

const useUserLatestAlbums = (id, limit) => useQuery(
    ['useUserLatestAlbums', id], 
    () => getUserLatestAlbums(id, limit)
);

export default useUserLatestAlbums;
