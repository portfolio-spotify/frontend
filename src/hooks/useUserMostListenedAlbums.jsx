import { useQuery } from '@tanstack/react-query';

const getUserMostListenedAlbums = async (id, limit) => {
    const limitParam = limit ? `?limit=${limit}` : '';

    const response = await fetch(`/api/users/${id}/projects/popular${limitParam}`);

    const responseJson = await response.json();

    if (!response.ok) return Promise.reject(responseJson);

    return responseJson;
};

const useUserMostListenedAlbums = (id, limit) => useQuery(
    ['useUserMostListenedAlbums', id], 
    () => getUserMostListenedAlbums(id, limit)
);

export default useUserMostListenedAlbums;
