import { useQuery } from '@tanstack/react-query';

const getTechnologyMostListenedAlbums = async (id, limit) => {
    const limitParam = limit ? `?limit=${limit}` : '';

    const response = await fetch(`/api/technologies/${id}/projects/popular${limitParam}`);

    const responseJson = await response.json();

    if (!response.ok) return Promise.reject(responseJson);

    return responseJson;
};

const useTechnologyMostListenedAlbums = (id, limit) => useQuery(
    ['technologyMostListenedAlbums', id], 
    () => getTechnologyMostListenedAlbums(id, limit)
);

export default useTechnologyMostListenedAlbums;
