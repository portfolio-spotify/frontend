import { useQuery } from '@tanstack/react-query';

const getUserTotalAlbums = async (id) => {
    const response = await fetch(`/api/users/${id}/projects/total`);

    const responseJson = await response.json();

    if (!response.ok) return Promise.reject(responseJson);

    return responseJson;
};

const useUserTotalAlbums = (id) => useQuery(
    ['useUserTotalAlbums', id], 
    () => getUserTotalAlbums(id)
);

export default useUserTotalAlbums;
