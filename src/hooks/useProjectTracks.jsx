import { useQuery } from '@tanstack/react-query';

const getProjecTracks = async (projectId) => {
    const response = await fetch(`/api/projects/${projectId}/tracks`);

    const responseJson = await response.json();

    if (!response.ok) return Promise.reject(responseJson);

    return responseJson;
};

const useProjectTracks = (projectId) => {
    const results = useQuery({
        queryKey: ['projects', 'tracks', projectId],
        queryFn: () => getProjecTracks(projectId),
        enabled: !!projectId
    });

    return results;
};

export default useProjectTracks;