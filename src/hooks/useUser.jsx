import { useQuery } from '@tanstack/react-query';

const getUser = async (userId) => {
    const response = await fetch(`/api/users/${userId}`);

    const responseJson = await response.json();

    if (!response.ok) return Promise.reject(responseJson);

    return responseJson;
};

// const useUser = (id) => {
//     const { isLoading, error, data } = useQuery({
//         queryKey: ['user', id],
//         queryFn: () => getUser(id),
//         enabled: !!id
//     });
// };

const useUser = (userId) => {
    const results = useQuery({
        queryKey: ['user', userId],
        queryFn: () => getUser(userId),
        enabled: !!userId
    });

    return results;
};

export default useUser;