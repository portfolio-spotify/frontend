import styles from './UserBanner.module.css'

import { ReactComponent as LogoCertified } from '../../../assets/icons/certified.svg';

const UserBanner = (props) => {
    const { coverImage, name, totalProjects } = props;

    return (
        <header className={styles.banner}>
            <div className={styles.background}>
                <img src={coverImage} alt="background profile" />
            </div>
            <section className={styles.details}>
                <div className={styles.certified}>
                    <LogoCertified />
                    <span>Développeur vérifié</span>
                </div>
                <h1 className={styles.name}>{name}</h1>
                <span className={styles.listeners}>{totalProjects} projets ajoutés</span>
            </section>
        </header>
    );
};

export default UserBanner;