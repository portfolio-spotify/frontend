import ButtonPrimary from '../../../components/Buttons/ButtonPrimary';

import styles from './UserActions.module.css';

import { ReactComponent as LogoMenu } from '../../../assets/icons/menu.svg';

const UserActions = () => {
    return (
        <section className={styles.actions}>
            <ButtonPrimary size="big"/>
            <button className={styles.subscribe}>
                s'abonner
            </button>
            <LogoMenu className={styles.menu} />
        </section>
    );
};

export default UserActions;