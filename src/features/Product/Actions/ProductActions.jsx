import { useState, useEffect } from 'react';

import ButtonPrimary from '../../../components/Buttons/ButtonPrimary';
import useAudioPlayerStore from '../../../stores/useAudioPlayerStore';

import styles from './ProductActions.module.css';

import { ReactComponent as LikeLineIcon } from './../../../assets/icons/like-line.svg';
import { ReactComponent as LikeFillIcon } from './../../../assets/icons/like-fill.svg';
import { ReactComponent as MenuIcon } from './../../../assets/icons/menu.svg';

const ProductActions = (props) => {
    const { album, tracks, user } = props;

    const isPlaying = useAudioPlayerStore((state) => state.isPlaying);
    const currentPlayingAlbum = useAudioPlayerStore((state) => state.album);
    const likedAlbums = useAudioPlayerStore((state) => state.likedAlbums);
    const setIsPlaying = useAudioPlayerStore((state) => state.setIsPlaying);
    const setAlbum = useAudioPlayerStore((state) => state.setAlbum);
    const setTracks = useAudioPlayerStore((state) => state.setTracks);
    const setAuthor = useAudioPlayerStore((state) => state.setAuthor);
    const setTrackIndex = useAudioPlayerStore((state) => state.setTrackIndex);
    const setLikedAlbums = useAudioPlayerStore((state) => state.setLikedAlbums);
    const [isAlbumPlaying, setIsAlbumPlaying] = useState(false);
    const [isLiked, setIsLiked] = useState(false);

    /* Update current album like status */
    useEffect(() => {
        if (likedAlbums) {
            const savedAlbums = new Set(likedAlbums);

            const isAlbumLiked = savedAlbums.has(album.id);

            if (isAlbumLiked) return setIsLiked(true);

            return setIsLiked(false);
        }

        setIsLiked(false);
    }, [likedAlbums, album.id]);

    /* Update current album playing status */
    useEffect(() => {
        if (isPlaying) {
            const isCurrentAlbumPlaying = album.id === currentPlayingAlbum.id;

            if (isCurrentAlbumPlaying) return setIsAlbumPlaying(true);
        }

        setIsAlbumPlaying(false);
    }, [isPlaying, album.id, currentPlayingAlbum.id]);

    const togglePlayPause = () => {
        const isCurrentAlbumPlaying = album.id === currentPlayingAlbum.id;

        if (isPlaying && isCurrentAlbumPlaying) return setIsPlaying(false);

        if (!isPlaying && isCurrentAlbumPlaying) return setIsPlaying(true);

        setAlbum(album);
        setTracks(tracks);
        setAuthor(user);
        setIsPlaying(true);
        setTrackIndex(0);
    };

    const onLike = () => {
        if (isLiked) {
            const savedLikes = new Set(likedAlbums);
            savedLikes.delete(album.id);
            setLikedAlbums(savedLikes);
            return setIsLiked(false);
        }

        const savedLikes = new Set(likedAlbums);
        savedLikes.add(album.id);
        setLikedAlbums(savedLikes);
        return setIsLiked(true);
    };

    const likeIconStyle = isLiked ? `${styles.like} ${styles.active}` : `${styles.like}`;

    return (
        <section className={styles.actions}>
            <ButtonPrimary size="big" onClick={togglePlayPause} isPlaying={isAlbumPlaying} />
            <button className={likeIconStyle} onClick={onLike}>
                {isLiked ? <LikeFillIcon /> : <LikeLineIcon />}
            </button>
            <MenuIcon className={styles.menu} />
        </section>
    );
};

export default ProductActions;