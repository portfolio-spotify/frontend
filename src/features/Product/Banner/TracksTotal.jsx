import styles from './ProductBanner.module.css';

const TracksTotal = (props) => {
    const { tracks } = props;

    if (!tracks?.length) return null;

    let text = '';

    if (tracks?.length === 1) text = `${tracks.length} titre`;

    if (tracks?.length > 1) text = `${tracks.length} titres`;

    return (
        <span className={styles.tracksTotal}>{text}</span>
    );
};

export default TracksTotal;