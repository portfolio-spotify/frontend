import { NavLink } from 'react-router-dom';
import capitalize from 'lodash/capitalize';

import styles from './ProductBanner.module.css';

import TracksTotal from './TracksTotal';
import { formatTotalDuration } from '../../../utils/date';

const ProductBanner = (props) => {
    const { album, type, tracks, user } = props;

    const artistName = `${capitalize(user.first_name)} ${capitalize(user.last_name)}`;
    const releaseYear = new Date(album.created_at).getFullYear();

    const inlineStyle = {
        background: `linear-gradient(to bottom, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.5)),
                     linear-gradient(${album.color}, ${album.color})`
    };

    const tracksLength = tracks.map((track) => Math.trunc(parseFloat(track.duration)));
    const totalDuration = tracksLength.reduce((partialSum, a) => partialSum + a, 0);
    const totalDurationFormat = formatTotalDuration(totalDuration);

    return (
        <header className={styles.banner} style={inlineStyle}>
            <img src={album.imageURL} alt="cover album" className={styles.cover} />
            <section className={styles.details}>
                <span className={styles.type}>{type}</span>
                <h1>{album.title}</h1>
                <p className={styles.description}>{album.description}</p>
                <div className={styles.artist}>
                    <NavLink to={`/artist/${user.url}`} className={styles.artistLink}>
                        <img src={user.image_profileURL} alt="artist" className={styles.artistPhoto} />
                        {artistName}
                    </NavLink>
                    <span className={styles.releaseDate}>{releaseYear}</span>
                    <TracksTotal tracks={tracks} />
                    <span>,</span>
                    <span className={styles.lengthTotal}>{totalDurationFormat}</span>
                </div>
            </section>
        </header>
    );
};

export default ProductBanner;